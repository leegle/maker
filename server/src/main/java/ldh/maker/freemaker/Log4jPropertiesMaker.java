package ldh.maker.freemaker;

/**
 * Created by ldh on 2017/4/8.
 */
public class Log4jPropertiesMaker extends FreeMarkerMaker<Log4jPropertiesMaker> {

    protected String projectRootPackage;

    public Log4jPropertiesMaker projectRootPackage(String projectRootPackage) {
        this.projectRootPackage = projectRootPackage;
        return this;
    }

    @Override
    public Log4jPropertiesMaker make() {
        data();
        this.out("log4jProperties.ftl", data);
        return this;
    }

    @Override
    public void data() {
        fileName = "log4j.properties";
        data.put("projectRootPackage", projectRootPackage);
    }
}
