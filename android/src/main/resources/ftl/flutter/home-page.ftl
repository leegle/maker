import 'package:flutter/material.dart';
import 'data-table.dart';

final List<_Page> _pages = [
  new _Page(icon: new Icon(Icons.ac_unit), title: "主界面"),
  new _Page(icon: new Icon(Icons.access_alarms), title: "监控页面"),
  new _Page(icon: new Icon(Icons.accessible_forward), title: "报警页面"),
  new _Page(icon: new Icon(Icons.account_box), title: "报警页面2"),
];

class HomePage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {

  int _currentPageIndex = 0;
  PageController _pageController = new PageController();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("代码自动生成flutter"),
        actions: <Widget>[
          new PopupMenuButton(itemBuilder: (build) {
            return [new PopupMenuItem(child: new Text("exit!"))];
          })
        ],
      ),
      body: new PageView.builder(
          itemCount: _pages.length,
          controller: _pageController,
          itemBuilder: (BuildContext context, int index) {
            return new _Body(index: index);
          }),
      bottomNavigationBar: new BottomNavigationBar(
        currentIndex: _currentPageIndex,
        type: BottomNavigationBarType.shifting,
        onTap: (int index) {
          _pageController.animateToPage(index, duration: new Duration(seconds: 2),curve:new ElasticOutCurve(0.8));
          _pageChange(index);
        },
        items: _pages.map((_Page _page) {
          return new BottomNavigationBarItem(icon: _page.icon, title: new Text(_page.title), backgroundColor: Colors.blue);
        }).toList(),
      ),
    );
  }

  void _pageChange(int index) {
    setState(() {
      if (_currentPageIndex != index) {
        _currentPageIndex = index;
      }
    });
  }
}

class _Body extends StatelessWidget {
  const _Body({Key key, this.index}) : super(key: key);

  final int index;

  @override
  Widget build(BuildContext context) {
    if (index == 0) {
      return
        new GridView.count(
          crossAxisCount: 3,
          padding: EdgeInsets.only(left: 5.0, right: 5.0, top: 10.0, bottom: 5.0),
          mainAxisSpacing: 5.0,
          crossAxisSpacing: 5.0,
          children: <Widget>[
            _BeanItem(icon: new Icon(Icons.access_alarm, size: 56.0, color: Colors.blueAccent,), title: "用户管理"),
            _BeanItem(icon: new Icon(Icons.title, size: 56.0, color: Colors.blueAccent,), title: "教师管理"),
            _BeanItem(icon: new Icon(Icons.star, size: 56.0, color: Colors.blueAccent,), title: "学生管理"),
            _BeanItem(icon: new Icon(Icons.cake, size: 56.0, color: Colors.blueAccent,), title: "课程管理"),
            _BeanItem(icon: new Icon(Icons.backspace, size: 56.0, color: Colors.blueAccent,), title: "班级管理"),
          ],
        );
    } else if (index == 1) {
      return new Center(child: new Text("page 2"),);
    } else {
      return new Center(child: new Text("page $index" ),);
    }
  }

}

class _BeanItem extends StatelessWidget {
  const _BeanItem({Key key, this.icon, this.title}) : super(key: key);
  final Icon icon;
  final String title;

  @override
  Widget build(BuildContext context) {
    return new Card(
        child: new RaisedButton(
          onPressed: () {
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) {
                    return new DataTableDemo();
                })
            );
          },
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              icon,
              new Text(title,
                style: new TextStyle(
                  color: Colors.blueAccent,
                  fontSize: 16.0,
                ),
                overflow: TextOverflow.clip,
                maxLines: 1,
              ),
            ],
          ),
        )
    );
  }

}

class _Page {
  const _Page({this.icon, this.title});

  final Icon icon;
  final String title;
}