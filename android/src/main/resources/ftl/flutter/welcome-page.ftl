import 'package:flutter/material.dart';
import '../util/window-util.dart';
import '../util/interface.dart';
import 'package:shared_preferences/shared_preferences.dart';

final List<String> _images = [
  'images/wallpapers/wallpaper1.jpg',
  'images/wallpapers/wallpaper2.jpg',
  'images/wallpapers/wallpaper3.jpg',
];

class WelcomePage extends StatefulWidget {
  WelcomePage({Key key, this.callback}) : super(key: key);

  final Callback callback;

  @override
  State<StatefulWidget> createState() {
    return _Welcome();
  }
}

enum _RadioGroup {
  page1,
  page2,
  page3,
}

class _Welcome extends State<WelcomePage> {

  PageController _pageController = new PageController();

  _RadioGroup _radioType = _RadioGroup.page1;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
          child: new SizedBox.fromSize(
              size: Size.fromHeight(getScreenHeight(context)),
              child: new PageView.builder(
                  itemCount: _images.length,
                  onPageChanged: _pageChanged,
                  controller: _pageController,
                  itemBuilder: (BuildContext context, int index) {
                    return new _GuidePage(index: index, callback: widget.callback);
                  })
          )
      ),
      floatingActionButton: new Padding(
        padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 100.0),
        child: new Card(
            child: new Container(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Radio<_RadioGroup>(value: _RadioGroup.page1,
                      groupValue: _radioType,
                      onChanged: _ratioChanged,
                      activeColor: Colors.redAccent),
                  new Radio<_RadioGroup>(value: _RadioGroup.page2,
                      groupValue: _radioType,
                      onChanged: _ratioChanged,
                      activeColor: Colors.redAccent),
                  new Radio<_RadioGroup>(value: _RadioGroup.page3,
                    groupValue: _radioType,
                    onChanged: _ratioChanged,
                    activeColor: Colors.redAccent,),
                ],
              ),
              decoration: new BoxDecoration(
                color: Colors.lightBlueAccent[100],
              ),
            )
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  void _pageChanged(int value) {
    print("value333:$value");
    _RadioGroup v = _RadioGroup.page1;
    if (value == 1) v = _RadioGroup.page2;
    if (value == 2) v = _RadioGroup.page3;
    setState(() {
      _radioType = v;
    });
  }

  void _ratioChanged(_RadioGroup value) {
    print("value:$value");
    print("_radioType:$_radioType");
    int v = 0;
    if (value == _RadioGroup.page2) v = 1;
    if (value == _RadioGroup.page3) v = 2;
    _pageController.animateToPage(
        v, duration: new Duration(seconds: 1), curve: new ElasticOutCurve(0.8));
    setState(() {
      _radioType = value;
    });
  }
}

class _GuidePage extends StatelessWidget {
  const _GuidePage({Key key, this.index, this.callback});

  final int index;
  final Callback callback;

  @override
  Widget build(BuildContext context) {
    if (index != _images.length - 1) {
      return new Image.asset(_images[index], fit: BoxFit.cover);
    }
    return new Stack(
      children: <Widget>[
        new SizedBox.fromSize(
          size: Size.fromHeight(getScreenHeight(context)),
          child: new Image.asset(_images[index], fit: BoxFit.cover),
        ),
        new Padding(
          padding: EdgeInsets.only(top: 100.0, right: 50.0),
          child: new Align(
              alignment: Alignment.topRight,
              child: new Card(
                child: new Container(
                    padding: EdgeInsets.all(10.0),
                    child: new FlatButton(
                      onPressed: () {
                        _endGuide(callback);
                      },
                      child: new Text("进入", style: new TextStyle(
                        color: Colors.red,
                        fontSize: 24.0,
                      )),)
                ),
              )
          ),
        ),
      ],
    );
  }

  _endGuide(Callback callback) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool("isGuide", true);
    callback();
  }
}