package ldh.maker.freemaker;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainControllerMaker extends FreeMarkerMaker<MainControllerMaker> {

	protected String controllerPackage;
	protected String author;

	public MainControllerMaker() {

	}

	public MainControllerMaker controllerPackage(String controllerPackage) {
		this.controllerPackage = controllerPackage;
		return this;
	}


	public MainControllerMaker author(String author) {
		this.author = author;
		return this;
	}

	public void data() {
		fileName = "MainController.java";
		data.put("fileName", fileName);
		data.put("controllerPackage", controllerPackage);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String str=sdf.format(new Date());
		data.put("Author", author);
		data.put("DATE", str);
		check();
	}

	public void check() {
		super.check();
	}
	
	@Override
	public MainControllerMaker make() {
		data();
		out("/easyui/mainController.ftl", data);
		
		return this;
	}
}
